package soft.tcamcaho.model.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import soft.tcamacho.model.Rebel;

/**
 *
 * @author soft PetAlert
 */
public class RebelDao {

    private final DbConnect dbConnect;
    private final String QUERY_INSERT_REBELS = "insert into rebels(name, planet) values (?,?)";
    private final String QUERY_SELECT_ALL = "select * from rebels";
    
    /**
     * Instance for connection in data base
     */
    public RebelDao() {
        dbConnect = DbConnect.getInstance();
    }

    /**
     * Insert in data base one object rebel, recived one list
     *
     * @param r of insert
     * @return rows insert ok or 0 it's not insert
     */
    public int insertRebels(Rebel r) {
        int rowsAffected = 0;
        if (r != null) {
            try (Connection connection = dbConnect.getConnection()) {
                if (connection != null) {
                    PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_REBELS);

                    statement.setString(1, r.getName());
                    statement.setString(2, r.getPlanet());
                    //statement.setLong(3, r.getDate());

                    rowsAffected = statement.executeUpdate();
                }
            } catch (SQLException ex) {
                rowsAffected = 0;
                System.out.println(ex.toString());
            }
        }
        return rowsAffected;
    }

    /**
     * selects all Rebel from database
     *
     * @return list all rebels in the database or null in case of error
     */
    public List<Rebel> findAllRebels() {
        List<Rebel> found = null;
        try (Connection conn = dbConnect.getConnection()) { //mirar doc para ver como funciona este try
            if (conn != null) {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(QUERY_SELECT_ALL);
                found = new ArrayList<>(); 
                while (rs.next()) { 
                    Rebel rebel = resultSetToRebel(rs); 
                    found.add(rebel); 
                }
            }
        } catch (SQLException ex) {
            found = null;
        }
        return found;
    }

    /**
     * gets data from curren registrer of result set and converse into a rebel
     * object.
     *
     * @param rs resultset to get data from
     * @return a rebel with data in resultset
     */
    private Rebel resultSetToRebel(ResultSet rs) throws SQLException { 
        //int id = rs.getInt("id");
        String name = rs.getString("name"); 
        String planet = rs.getString("planet");
        long creationDate = rs.getLong("creationDate");
        
        Rebel rebel = new Rebel( name, planet, creationDate);

        return rebel;
    }
}
