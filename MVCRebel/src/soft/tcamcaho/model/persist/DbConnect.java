
package soft.tcamcaho.model.persist;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Tamara Camacho
 */
public class DbConnect {
    
    private static DbConnect instance = null;
    
    private final String DRIVER = "com.mysql.jdbc.Driver";  
    private final String BD_URL = "jdbc:mysql://" + "localhost/concatel";
    private final String USUARI = "concatel";
    private final String PASSWORD = "concatel";
    
    

    private DbConnect() {
        try{
            Class.forName(DRIVER);
        }catch(ClassNotFoundException e){
        System.err.append("Instalation error. Contact to the admin");
        System.exit(-1);
    }
        
    }
    /**
     * Singelton
     * Instance for conection to the data base
     * @return instance conection ok or null it's conection ko
     */
    public static DbConnect getInstance(){
        if(instance == null){
            instance = new DbConnect();
        }
        return instance;
    }
    
    /**
     * Conection for the data base
     * @return the ok connection or null connection
     */
    public Connection getConnection(){
        Connection conn = null;
        try{
           conn = DriverManager.getConnection(BD_URL,USUARI,PASSWORD);
        }catch(SQLException e){
            conn = null;
        }
        return conn;
    }
    
}
