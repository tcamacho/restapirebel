package soft.tcamacho.model;

/**
 *
 * @author tcamacho
 */
public class Rebel {
    private int id;
    private String name;
    private String planet;
    private long timeInMillis;

    public Rebel() {
    }

    public Rebel(int id) {
        this.id = id;
    }

    
    public Rebel(int id, String name, String planet, long timeInMillis) {
        this.id = id;
        this.name = name;
        this.planet = planet;
        this.timeInMillis = timeInMillis;
    }

    public Rebel(String name, String planet, long timeInMillis) {
        this.name = name;
        this.planet = planet;
        this.timeInMillis = timeInMillis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    public long getDate() {
        return timeInMillis;
    }

    public void setDate(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rebel other = (Rebel) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Rebel{");
        sb.append("id= ").append(id);
        sb.append(", name= ").append(name);
        sb.append(", planet= ").append(planet);
        sb.append(", date= ").append(timeInMillis);
        
        return sb.toString();
    }
}
