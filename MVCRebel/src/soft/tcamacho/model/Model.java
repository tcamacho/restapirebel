package soft.tcamacho.model;

import java.util.List;
import soft.tcamcaho.model.persist.RebelDao;

/**
 *
 * @author tcamacho
 */
public class Model {
    
    private final RebelDao rebelDao;

    public Model() {
        rebelDao = new RebelDao();
    }

    public int insertRebels(Rebel r) {
        int resultCode ;
        if(r != null){
            resultCode = rebelDao.insertRebels(r);
        }else{
            resultCode = 0;
        }
        
        return resultCode;
    }
    
    public List<Rebel> findAllRebels() {
        List <Rebel> resultCode; 
        resultCode = rebelDao.findAllRebels();
        return resultCode;
    }
    
    
}
