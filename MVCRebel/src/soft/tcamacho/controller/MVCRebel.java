package soft.tcamacho.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import soft.tcamacho.model.Model;
import soft.tcamacho.model.Rebel;

/**
 *
 * @author tcamacho
 */
public class MVCRebel {

    private Model model;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MVCRebel app = new MVCRebel();
        app.run();
    }

    private void run() {
        model = new Model();
        loadRebels();
    }

    private void loadRebels() {
        List <Rebel> rebels = new ArrayList();
        
        rebels.add(new Rebel("Frodo", "Venus", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Holly", "Marte", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Courtney", "Mercurio", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Amber", "Saturno", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Caitlin", "Júpiter", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Charlotte", "Tierra", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Louis", "Pluton", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Ceres", "Neptuno", Calendar.getInstance().getTimeInMillis()));
        rebels.add(new Rebel("Eric", "Urano", Calendar.getInstance().getTimeInMillis()));
        
        for (Rebel r : rebels) {
            int resultCode = model.insertRebels(r);
            System.out.println(r.getId() + " " + resultCode);
        }
        
    }
    
}
